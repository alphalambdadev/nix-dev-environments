{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "al";
  home.homeDirectory = "/home/al";

  imports = [
    ./common/default.nix
    ./xsession/default.nix
    ./programs/alacritty.nix
    ./programs/neovim.nix
  ];
}
