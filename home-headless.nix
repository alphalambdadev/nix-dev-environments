{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "wrk";
  home.homeDirectory = "/home/wrk";

  imports = [
    ./common/default.nix
    ./programs/neovim.nix
  ];
}
