{ config, pkgs, ... }:

{
  xsession = {
    enable = true;
    windowManager.xmonad = {
      enable = true;
      config = pkgs.writeText "xmonad.hs" ''
import           XMonad


myConfig = def
  { modMask     = mod4Mask -- set 'Mod' to windows key
  , terminal    = "alacritty"
  }
main :: IO ()
main = xmonad myConfig
      '';
    };
  };
}
